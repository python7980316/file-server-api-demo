## Description

### Simple file server with REST API

REST API JSON Python microservice which allows users to upload, download, delete 
and list files and some file stats.

### Technologies
* APP
  * Python 3.11
  * FastAPI 0.95
* Web server
  * Uvicorn 0.22
  * Nginx 1.23
* Database
  * PostgreSQL 15
  * Adminer (for GUI administration)

## Development
1. Install Docker.
2. (optional) Create config file to change default environment variables from `/.env.example` file.
   ```
   cp ./.env.example ./.env
   ```
   * API
     * prefix - `/api/v1`
   * DB credentials
     * `user` / `password` / `app_db` 
   * app ports:
     * `NGINX_PORT` (app's API) - `80` 
     * `ADMINER_PORT` (DB admin) - `8080`
   * environment: 
     * mainly used for setting logging level
       * `ENVIRONMENT=development` (default)
         * `DEBUG` level for APP
         * shows also DB engine logs
       * other
         * `INFO` level for APP
3. Run container setup.
   ```
   docker-compose up -d
   ```
4. Voilà!
   * app's API documentation is present by default on:
     * http://localhost/docs
   * DB admin:
     * http://localhost:8080
   * tests:
     ```
     docker-compose exec app pytest --rootdir=tests
     ```
