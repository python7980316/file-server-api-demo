import asyncio

import pytest
from asgi_lifespan import LifespanManager
from fastapi import FastAPI

from app.api.app import create_app, init_app_environment
from app.core.config import DbSettings, Environment, Settings
from app.core.container import AppContainer


def _settings_local_partial() -> DbSettings:
    return DbSettings(
        ENVIRONMENT=Environment.TEST,
        HOST='db-test',
    )


@pytest.fixture
async def app(di_container: AppContainer) -> FastAPI:
    app = create_app(di_container)

    async with LifespanManager(app):
        yield app


@pytest.fixture(scope='session')
async def di_container(db_settings: Settings) -> AppContainer:
    container = init_app_environment(db_settings)

    yield container

    await _cleanup_db(container)


@pytest.fixture(scope='session')
async def db_settings() -> Settings:
    db_settings = DbSettings()
    for field, value in _settings_local_partial():
        setattr(db_settings, field, value)

    return db_settings


async def _cleanup_db(di_container: AppContainer) -> None:
    await di_container.database().reset_db_schema()


@pytest.fixture(scope='session')
def event_loop():
    """
    Redefine event_loop fixture for used scope
    """
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()
