from http import HTTPStatus

from httpx import AsyncClient

from app.api.v1.schema.file_stats import FileStatsResponseSchema


# @TODO fix test dependency - pytest is skipping all tests with dependencies
# @pytest.mark.dependency(name='test_get_file_stats_ok', depends=['test_upload_file_ok'])
async def test_get_file_stats_ok(http_client_for_file_stats: AsyncClient):
    response = await http_client_for_file_stats.get('/')
    assert response.status_code == HTTPStatus.OK

    data = response.json()
    assert isinstance(data, dict)

    data = FileStatsResponseSchema(**data)
    assert data.count_total == 1
