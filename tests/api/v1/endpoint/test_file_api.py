from http import HTTPStatus

import pytest
from httpx import AsyncClient


@pytest.mark.parametrize('data', [
    {'uploaded_file': 'string'},
])
async def test_upload_file_validation_error(http_client_for_files: AsyncClient, data: dict):
    response = await http_client_for_files.post('/')

    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


@pytest.mark.dependency(name='test_upload_file_ok')
@pytest.mark.parametrize('file', [
    {'file': open('tests/api/v1/endpoint/files/loading.gif', 'rb')},
])
async def test_upload_file_ok(http_client_for_files: AsyncClient, file: dict):
    response = await http_client_for_files.post('/', files=file)

    assert response.status_code == HTTPStatus.CREATED
