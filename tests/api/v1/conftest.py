from collections.abc import AsyncIterator
from contextlib import asynccontextmanager

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from app.core.config import ApiSettings


@asynccontextmanager
async def _generate_http_client(app: FastAPI, resource_base_path: str) -> AsyncIterator[AsyncClient]:
    api_settings = app.state.container.settings()
    assert isinstance(api_settings, ApiSettings)

    async with AsyncClient(
            app=app,
            base_url='http://localhost:8000' + api_settings.V1_PREFIX + resource_base_path,
            follow_redirects=True,
    ) as client:
        yield client


@pytest.fixture
async def http_client_for_files(app: FastAPI) -> AsyncClient:
    async with _generate_http_client(app, '/files') as client:
        yield client


@pytest.fixture
async def http_client_for_file_stats(app: FastAPI) -> AsyncClient:
    async with _generate_http_client(app, '/file-stats') as client:
        yield client
