from app.database.database import Database
from app.database.model.file_stats import FileStats
from app.database.repository.file import FileRepository


class FileStatsService:
    def __init__(self, database: Database, repository: FileRepository):
        self._database = database
        self._repository = repository

    async def get_file_size_aggregate_functions_with_count(self) -> FileStats:
        async for db_session in self._database.generate_db_session():
            return await self._repository.fetch_all_with_size_aggregate_functions_with_count(db_session)
