from collections.abc import Sequence

from app.api.v1.schema.file import FileCreateSchema, FileDeleteSchema, FileReadSchema
from app.database.database import Database
from app.database.model.file import FileBase, FileCreateModel
from app.database.repository.file import FileRepository


class FileService:
    def __init__(self, database: Database, repository: FileRepository):
        self._database = database
        self._repository = repository

    # @TODO move file contents away from DB (e.g. to filesystem) and replace respective field with link to file

    async def get_files(self) -> list[FileBase]:
        async for db_session in self._database.generate_db_session():
            return await self._repository.fetch_all_without_deferred_fields(db_session)

    async def get_file(self, file_schema: FileReadSchema) -> FileBase | None:
        async for db_session in self._database.generate_db_session():
            return await self._repository.fetch_one_without_deferred_fields(db_session, file_schema.id)

    async def get_file_with_contents(self, file_schema: FileReadSchema) -> FileBase | None:
        async for db_session in self._database.generate_db_session():
            return await self._repository.fetch_one_with_all_fields(db_session, file_schema.id)

    async def create_file(self, file_schema: FileCreateSchema) -> Sequence:
        async for db_session in self._database.generate_db_session():
            file = FileCreateModel.from_orm(file_schema)

            return await self._repository.create(db_session, file)

    async def delete_file(self, file_schema: FileDeleteSchema) -> None:
        async for db_session in self._database.generate_db_session():
            await self._repository.delete(db_session, file_schema.id)
