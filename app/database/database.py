from collections.abc import AsyncIterator

from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlmodel import SQLModel

from app.core.config import DbSettings
from app.database.session import DbSession


class Database:

    def __init__(self, settings: DbSettings):
        self._engine = None
        self._settings = settings

    def _get_db_url_async(self) -> str:
        return f'postgresql+asyncpg://{self._settings.USER}:{self._settings.PASSWORD}' \
               f'@{self._settings.HOST}/{self._settings.NAME}'

    def _get_engine(self) -> AsyncEngine:
        if self._engine is None:
            self._engine = create_async_engine(
                self._get_db_url_async(),
                echo=self._settings.is_debug_logging_enabled(),  # ENV-specific SQL logs
            )

        return self._engine

    async def init_db_schema(self) -> None:
        engine = self._get_engine()
        async with engine.begin() as connection:
            await connection.run_sync(SQLModel.metadata.create_all)

    async def reset_db_schema(self) -> None:
        engine = self._get_engine()
        async with engine.begin() as connection:
            await connection.run_sync(SQLModel.metadata.drop_all)

    async def generate_db_session(self) -> AsyncIterator[DbSession]:
        engine = self._get_engine()
        async_session = sessionmaker(
            engine,
            class_=AsyncSession,
            expire_on_commit=False,
        )

        async with async_session() as session:
            try:
                db_session = DbSession(session)
                yield db_session
            finally:
                await db_session.shutdown()
