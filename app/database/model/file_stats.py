from pydantic import BaseModel


class FileStats(
    BaseModel
):
    count_total: int = 0
    size_total_in_bytes: int = 0
    size_average_in_bytes: int = 0
    size_median_in_bytes: int = 0
