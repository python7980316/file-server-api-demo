from sqlmodel import SQLModel

from app.database.model.common import TimestampCreatedModel, UUIDIdModel, FileContentsModel


class FileBase(
    SQLModel
):
    name: str
    media_type: str
    size_in_bytes: int


class FileCreateModel(
    FileContentsModel,
    TimestampCreatedModel,
    FileBase,
):
    ...


class File(
    FileCreateModel,
    UUIDIdModel,
    table=True
):
    ...
