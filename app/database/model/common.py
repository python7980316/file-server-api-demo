from datetime import datetime
from uuid import UUID, uuid4

from sqlmodel import SQLModel, Field, Column


class UUIDIdModel(SQLModel):
    id: UUID = Field(
        default_factory=uuid4,
        primary_key=True,
        nullable=False,
    )


class TimestampCreatedModel(SQLModel):
    created: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
    )


class FileContentsModel(SQLModel):
    contents: bytes = Column(
        sql_type='bytea',
    )
