from collections import namedtuple
from collections.abc import Sequence
from uuid import UUID

from sqlalchemy import delete, func, insert
from sqlalchemy.future import select
from sqlalchemy.orm import defer

from app.database.model.file import File, FileBase
from app.database.model.file_stats import FileStats
from app.database.session import DbSession


class FileRepository:
    table = File

    async def fetch_all_without_deferred_fields(self, db_session: DbSession) -> list[FileBase]:
        query = select(self.table).options(defer(self.table.contents))

        return await db_session.fetch_all(query)

    async def fetch_one_without_deferred_fields(self, db_session: DbSession, file_id: UUID) -> FileBase | None:
        query = select(self.table).options(defer(self.table.contents)).filter(self.table.id == file_id)

        return await db_session.fetch_one(query)

    async def fetch_one_with_all_fields(self, db_session: DbSession, file_id: UUID) -> FileBase | None:
        query = select(self.table).filter(self.table.id == file_id)

        return await db_session.fetch_one(query)

    async def create(self, db_session: DbSession, file: FileBase) -> Sequence:
        query = insert(self.table).values(file.dict()).returning(self.table)
        file = await db_session.create(query)
        await db_session.commit_transaction()

        return file

    async def delete(self, db_session: DbSession, file_id: UUID) -> None:
        query = delete(self.table).where(self.table.id == file_id)
        await db_session.delete(query)
        await db_session.commit_transaction()

    async def fetch_all_with_size_aggregate_functions_with_count(self, db_session: DbSession) -> FileStats:
        query = select(
            func.count(self.table.id).label('count_total'),
            func.sum(self.table.size_in_bytes).label('size_total'),
            func.avg(self.table.size_in_bytes).label('size_average'),
            func.percentile_cont(0.5).within_group(self.table.size_in_bytes).label('size_median'),
        )

        result = await db_session.fetch_values(query)
        assert result is not None  # nosec B101

        Result = namedtuple('Result', ['count_total', 'size_total', 'size_average', 'size_median'])
        result = Result(*result)

        if result.count_total < 1:
            return FileStats()

        file_stats = FileStats(
            count_total=result.count_total,
            size_total_in_bytes=result.size_total,
            size_average_in_bytes=result.size_average,
            size_median_in_bytes=result.size_median,
        )

        return file_stats
