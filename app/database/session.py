import logging
from typing import TypeVar

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import Delete, Insert, Selectable

T = TypeVar('T')


class DbSession:

    def __init__(self, session: AsyncSession):
        self._session = session

    async def commit_transaction(self) -> None:
        await self._session.commit()

    async def rollback_transaction(self) -> None:
        await self._session.rollback()

    async def shutdown(self) -> None:
        logging.debug('DB session shutdown')
        await self._session.close()

    async def fetch_all(self, query: Selectable) -> list[T]:
        result = await self._session.execute(query)

        return result.scalars().all()

    async def fetch_one(self, query: Selectable) -> T | None:
        result = await self._session.execute(query)

        return result.scalar_one_or_none()

    async def fetch_values(self, query: Selectable) -> T | None:
        result = await self._session.execute(query)

        return result.one_or_none()

    async def create(self, query: Insert) -> T:
        result = await self._session.execute(query)

        return result.one()

    async def delete(self, query: Delete) -> None:
        await self._session.execute(query)
