from http import HTTPStatus
from uuid import UUID

from fastapi import APIRouter, Depends, UploadFile
from fastapi.responses import Response

from app.api.exceptions import NotFoundHttpException
from app.api.v1.schema.file import FileCreateSchema, FileDeleteSchema, FileReadSchema, FileResponseSchema
from app.core.container import AppContainer, app_container
from app.database.model.file import File

router = APIRouter()


async def _get_uploaded_file_contents(file: UploadFile) -> bytes:
    return await file.read()


@router.post(
    '/',
    response_model=FileResponseSchema,
    status_code=HTTPStatus.CREATED,
)
async def upload_file(file: UploadFile, container: AppContainer = Depends(app_container)) -> FileResponseSchema:
    file_contents = await _get_uploaded_file_contents(file)
    file_schema = FileCreateSchema(
        name=file.filename,
        media_type=file.content_type,
        contents=file_contents,
        size_in_bytes=len(file_contents),
    )

    file_service = container.file_service()
    saved_file = await file_service.create_file(file_schema)

    return saved_file


@router.get(
    '/',
    response_model=list[FileResponseSchema],
    status_code=HTTPStatus.OK,
)
async def get_files_info(container: AppContainer = Depends(app_container)) -> list[FileResponseSchema]:
    file_service = container.file_service()
    file_list = await file_service.get_files()

    return file_list


@router.delete(
    '/{file_id}',
    status_code=HTTPStatus.NO_CONTENT,
    responses={
        404: {"description": "File not found"},
    },
)
async def delete_file(file_id: UUID, container: AppContainer = Depends(app_container)) -> None:
    """
    :raises NotFoundHttpException:
    """

    file_service = container.file_service()
    file_schema = FileReadSchema(
        id=file_id
    )
    file = await file_service.get_file(file_schema)
    if file is None:
        raise NotFoundHttpException('File not found')

    file_schema = FileDeleteSchema(
        id=file_id
    )
    await file_service.delete_file(file_schema)


@router.get(
    '/{file_id}/download',
    status_code=HTTPStatus.OK,
    response_class=Response,
    responses={
        404: {"description": "File not found"},
    },
)
async def download_file(file_id: UUID, container: AppContainer = Depends(app_container)) -> Response:
    """
    :raises NotFoundHttpException:
    """

    file_service = container.file_service()
    file_schema = FileReadSchema(
        id=file_id
    )
    file: File | None = await file_service.get_file_with_contents(file_schema)
    if file is None:
        raise NotFoundHttpException('File not found')

    return Response(
        content=file.contents,
        media_type=file.media_type
    )
