from http import HTTPStatus

from fastapi import APIRouter, Depends

from app.api.v1.schema.file_stats import FileStatsResponseSchema
from app.core.container import AppContainer, app_container

router = APIRouter()


@router.get(
    '/',
    response_model=FileStatsResponseSchema,
    status_code=HTTPStatus.OK,
)
async def get_file_stats(container: AppContainer = Depends(app_container)) -> FileStatsResponseSchema:
    file_stats_service = container.file_stats_service()
    file_stats = await file_stats_service.get_file_size_aggregate_functions_with_count()

    return file_stats
