from fastapi import APIRouter

from app.api.v1.endpoint import file, file_stats

api_router = APIRouter()
api_router.include_router(file.router, prefix='/files')
api_router.include_router(file_stats.router, prefix='/file-stats')
