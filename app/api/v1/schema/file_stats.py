from pydantic import BaseModel


class FileStatsResponseSchema(
    BaseModel,
):
    count_total: int
    size_total_in_bytes: int
    size_average_in_bytes: int
    size_median_in_bytes: int
