from pydantic import BaseModel

from app.api.schema.common import TimestampCreatedModel, FileContentsModel, UUIDIdModel


class File(
    BaseModel
):
    name: str
    media_type: str
    size_in_bytes: int


class FileResponseSchema(
    TimestampCreatedModel,
    File,
    UUIDIdModel,
):
    ...


class FileCreateSchema(
    FileContentsModel,
    File,
):
    ...


class FileReadSchema(
    UUIDIdModel,
):
    ...


class FileDeleteSchema(
    UUIDIdModel,
):
    ...
