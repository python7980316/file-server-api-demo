from http import HTTPStatus

from fastapi import HTTPException


class NotFoundHttpException(HTTPException):
    def __init__(self, message='Item not found') -> None:
        super().__init__(status_code=HTTPStatus.NOT_FOUND, detail=message)
