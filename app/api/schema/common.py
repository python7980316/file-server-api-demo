from datetime import datetime
from uuid import UUID

from pydantic import BaseModel


class UUIDIdModel(BaseModel):
    id: UUID


class TimestampCreatedModel(BaseModel):
    created: datetime


class FileContentsModel(BaseModel):
    contents: bytes
