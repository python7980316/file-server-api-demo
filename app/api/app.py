import logging

from fastapi import APIRouter, FastAPI

from app.api.v1.api import api_router as api_v1_router
from app.core.config import ApiSettings, DbSettings, Settings
from app.core.container import AppContainer
from app.core.logger import init_logger
from app.database.database import Database


def init_app_environment(db_settings: Settings | None = None) -> AppContainer:
    if db_settings is None:
        db_settings = DbSettings()
    database = Database(db_settings)
    app_settings = ApiSettings()

    return AppContainer(database, app_settings)


def create_app(di_container: AppContainer | None = None) -> FastAPI:
    if di_container is None:
        di_container = init_app_environment()

    app = FastAPI(
        title='Simple file server',
    )
    app.state.container = di_container

    api_settings = app.state.container.settings()
    assert isinstance(api_settings, ApiSettings)  # nosec B101

    # @TODO add middleware for setting upload file size limit

    @app.on_event('startup')
    async def on_startup() -> None:
        init_logger(api_settings)
        logging.info('Application starting...')
        await app.state.container.startup()

    @app.on_event('shutdown')
    async def on_shutdown() -> None:
        logging.info('Application stopping...')
        await app.state.container.shutdown()

    app.include_router(api_v1_router, prefix=api_settings.V1_PREFIX)

    root_router = APIRouter()

    @root_router.get('/')
    async def healthcheck():
        return 'OK, working'

    app.include_router(root_router)

    return app
