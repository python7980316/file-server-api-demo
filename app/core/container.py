from fastapi import Request

from app.core.config import Settings
from app.database.database import Database
from app.database.repository.file import FileRepository
from app.service.file import FileService
from app.service.file_stats import FileStatsService


class AppContainer:

    def __init__(self, db: Database, settings: Settings) -> None:
        self._database = db
        self._settings = settings

    async def startup(self) -> None:
        await self._database.init_db_schema()

    async def shutdown(self) -> None:
        ...

    def database(self) -> Database:
        return self._database

    def settings(self) -> Settings:
        return self._settings

    def file_service(self) -> FileService:
        return FileService(self.database(), FileRepository())

    def file_stats_service(self) -> FileStatsService:
        return FileStatsService(self.database(), FileRepository())


def app_container(request: Request) -> AppContainer:
    container = getattr(request.app.state, 'container', None)
    assert isinstance(container, AppContainer)  # nosec B101

    return container
