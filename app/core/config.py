from enum import Enum

from pydantic import BaseSettings


class Environment(Enum):
    DEVELOPMENT = 'development'
    TEST = 'test'


class Settings(BaseSettings):
    ENVIRONMENT: Environment

    def is_debug_logging_enabled(self) -> bool:
        return True if self.ENVIRONMENT == Environment.DEVELOPMENT else False


class DbSettings(Settings):
    USER: str
    PASSWORD: str
    NAME: str
    HOST: str

    class Config:
        env_prefix = 'DB_'


class ApiSettings(Settings):
    V1_PREFIX: str

    class Config:
        env_prefix = 'API_'
