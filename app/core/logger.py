import logging

from app.core.config import Settings


def init_logger(settings: Settings):
    logging.basicConfig(
        level=logging.DEBUG if settings.is_debug_logging_enabled() else logging.INFO,  # ENV-specific app logs
        format='%(asctime)s %(levelname)s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            logging.StreamHandler()
        ]
    )
